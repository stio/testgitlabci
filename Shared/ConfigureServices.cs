﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using TestGitlabCi.Shared.Options;

namespace TestGitlabCi.Shared;

public static class ConfigureServices
{
    public static void RegisterSharedOptions(this IServiceCollection services, IConfiguration configuration)
    {
        services.Configure<AuthSettings>(configuration.GetSection(nameof(AuthSettings)));
    }
}