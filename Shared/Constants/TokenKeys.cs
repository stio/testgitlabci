﻿namespace TestGitlabCi.Shared.Constants;

public static class TokenKeys
{
    public const string UserId = "userId";

    public const string Email = "email";

    public const string Phone = "phone";

    public const string Role = "role";
}