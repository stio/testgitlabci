﻿namespace TestGitlabCi.Shared.Options;

public class AuthSettings
{
    public string? Authority { get; set; }

    public string? Audience { get; set; }
}