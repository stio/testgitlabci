﻿namespace TestGitlabCi.Domain.Enums;

public enum TodoListType
{
    Default,
    Specific,
}