﻿namespace TestGitlabCi.Domain.Enums;

public enum Role
{
    Client,
    Admin,
}