﻿using TestGitlabCi.Domain.Common;
using TestGitlabCi.Domain.Enums;

namespace TestGitlabCi.Domain.Entities;

public class TodoList : BaseEntity
{
    public Guid UserId { get; set; }

    public User? User { get; set; }

    public string? Name { get; set; }

    public TodoListType Type { get; set; }
}