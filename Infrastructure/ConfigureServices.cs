﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using TestGitlabCi.Application.Common.Interfaces;
using TestGitlabCi.Infrastructure.AuthorizationHandlers;
using TestGitlabCi.Infrastructure.Data;
using TestGitlabCi.Infrastructure.Services;
using TestGitlabCi.Shared.Constants;

namespace TestGitlabCi.Infrastructure;

public static class ConfigureServices
{
    public static void AddInfrastructureServices(this IServiceCollection services, IConfiguration configuration)
    {
        services.RegisterDbContext(configuration[ConnectionStrings.DefaultConnection]!);

        services.AddScoped<IResourceAuthorizationService, ResourceAuthorizationService>();
        services.AddScoped<IAuthorizationHandler, TodoListAuthorizationHandler>();

        services.AddScoped<ICurrentUserService, CurrentUserService>();
    }
}