﻿using Xunit;

namespace TestGitlabCi.Web.Api.IntegrationTests;

[CollectionDefinition(nameof(WebApp))]
public class WebAppCollection : ICollectionFixture<WebApp>
{
}