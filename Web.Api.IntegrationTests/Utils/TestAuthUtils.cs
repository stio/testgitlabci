﻿using System.Net.Http.Headers;
using System.Text.Json;
using TestGitlabCi.Domain.Entities;
using TestGitlabCi.Web.Api.IntegrationTests.TestAuth;
using TestGitlabCi.Web.Mocks.Authentication;

namespace TestGitlabCi.Web.Api.IntegrationTests.Utils;

public static class TestAuthUtils
{
    public static void AddToken(HttpClient client, User user)
    {
        var mockUser = new MockUserSettings()
        {
            Id = user.Id.ToString(),
            Email = user.Email,
            Role = user.Role.ToString(),
        };

        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(TestAuthAuthenticationOptions.DefaultScheme, JsonSerializer.Serialize(mockUser));
    }
}