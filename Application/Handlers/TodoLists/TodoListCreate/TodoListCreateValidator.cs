﻿using FluentValidation;
using TestGitlabCi.Application.Contracts.Requests;

namespace TestGitlabCi.Application.Handlers.TodoLists.TodoListCreate;

public class TodoListCreateValidator : AbstractValidator<TodoListCreateRequest>
{
    public TodoListCreateValidator()
    {
        this.RuleFor(x => x.Name)
            .MaximumLength(25);
    }
}