﻿using AutoMapper;
using MediatR;
using TestGitlabCi.Application.Common.Exceptions;
using TestGitlabCi.Application.Common.Interfaces;
using TestGitlabCi.Application.Contracts.Models;
using TestGitlabCi.Application.Contracts.Requests;
using TestGitlabCi.Domain.Entities;
using TestGitlabCi.Shared.Constants;

namespace TestGitlabCi.Application.Handlers.TodoLists.UpdateTodoList;

internal class TodoListUpdateHandler : IRequestHandler<TodoListUpdateRequest, TodoListDto>
{
    private readonly IApplicationDbContext dbContext;
    private readonly IResourceAuthorizationService resourceAuthorizationService;
    private readonly IMapper mapper;

    public TodoListUpdateHandler(
        IApplicationDbContext dbContext,
        IResourceAuthorizationService resourceAuthorizationService,
        IMapper mapper)
    {
        this.dbContext = dbContext;
        this.resourceAuthorizationService = resourceAuthorizationService;
        this.mapper = mapper;
    }

    public async Task<TodoListDto> Handle(TodoListUpdateRequest request, CancellationToken cancellationToken)
    {
        var todoList = await this.dbContext.TodoLists.FindAsync(request.Id);

        if (todoList is null)
        {
            throw new NotFoundException(nameof(TodoList), request.Id);
        }

        await this.resourceAuthorizationService.Authorize(todoList, Operations.FullAccess);

        this.mapper.Map(request, todoList);

        this.dbContext.TodoLists.Update(todoList);
        await this.dbContext.SaveChangesAsync(cancellationToken);

        return this.mapper.Map<TodoListDto>(todoList);
    }
}