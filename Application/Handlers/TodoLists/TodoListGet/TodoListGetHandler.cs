﻿using AutoMapper;
using MediatR;
using TestGitlabCi.Application.Common.Exceptions;
using TestGitlabCi.Application.Common.Interfaces;
using TestGitlabCi.Application.Contracts.Models;
using TestGitlabCi.Application.Contracts.Requests;
using TestGitlabCi.Domain.Entities;
using TestGitlabCi.Shared.Constants;

namespace TestGitlabCi.Application.Handlers.TodoLists.TodoListGet;

internal class TodoListGetHandler : IRequestHandler<TodoListGetRequest, TodoListDto>
{
    private readonly IApplicationDbContext dbsContext;
    private readonly IResourceAuthorizationService resourceAuthorizationService;
    private readonly IMapper mapper;

    public TodoListGetHandler(
        IApplicationDbContext dbsContext,
        IResourceAuthorizationService resourceAuthorizationService,
        IMapper mapper)
    {
        this.dbsContext = dbsContext;
        this.resourceAuthorizationService = resourceAuthorizationService;
        this.mapper = mapper;
    }

    public async Task<TodoListDto> Handle(TodoListGetRequest request, CancellationToken cancellationToken)
    {
        var todoList = await this.dbsContext.TodoLists.FindAsync(request.Id);

        if (todoList is null)
        {
            throw new NotFoundException(nameof(TodoList), request.Id);
        }

        await this.resourceAuthorizationService.Authorize(todoList, Operations.FullAccess);

        return this.mapper.Map<TodoListDto>(todoList);
    }
}