﻿using MediatR;
using TestGitlabCi.Application.Common.Interfaces;
using TestGitlabCi.Application.Contracts.Common;
using TestGitlabCi.Application.Contracts.Enums;
using TestGitlabCi.Application.Contracts.Models;
using TestGitlabCi.Application.Contracts.Requests;
using TestGitlabCi.Application.QueryableExtensions;

namespace TestGitlabCi.Application.Handlers.TodoLists.TodoListSearch;

internal class TodoListSearchHandler : IRequestHandler<TodoListSearchRequest, PagedList<TodoListRecordDto>>
{
    private readonly IApplicationDbContext dbsContext;
    private readonly ICurrentUserService currentUserService;

    public TodoListSearchHandler(
        IApplicationDbContext dbsContext,
        ICurrentUserService currentUserService)
    {
        this.dbsContext = dbsContext;
        this.currentUserService = currentUserService;
    }

    public Task<PagedList<TodoListRecordDto>> Handle(TodoListSearchRequest request, CancellationToken cancellationToken)
    {
        request.Sorting ??= new Sorting()
        {
            Column = nameof(TodoListRecordDto.CreatedAt),
            Direction = DirectionType.Desc,
        };

        return this.dbsContext.TodoLists
            .ForUser(this.currentUserService.UserId)
            .OrderByDescending(todoList => todoList.CreatedAt)
            .MapTodoListRecordDto()
            .ApplyFilter(request.Filter)
            .ApplySorting(request.Sorting)
            .ToPagedList(request.Pagination, cancellationToken);
    }
}