﻿using MediatR;
using TestGitlabCi.Application.Common.Exceptions;
using TestGitlabCi.Application.Common.Interfaces;
using TestGitlabCi.Application.Contracts.Requests;
using TestGitlabCi.Domain.Entities;
using TestGitlabCi.Shared.Constants;

namespace TestGitlabCi.Application.Handlers.TodoLists.TodoListDelete;

internal class TodoListDeleteHandler : IRequestHandler<TodoListDeleteRequest>
{
    private readonly IApplicationDbContext dbContext;
    private readonly IResourceAuthorizationService resourceAuthorizationService;

    public TodoListDeleteHandler(
        IApplicationDbContext dbContext,
        IResourceAuthorizationService resourceAuthorizationService)
    {
        this.dbContext = dbContext;
        this.resourceAuthorizationService = resourceAuthorizationService;
    }

    public async Task<Unit> Handle(TodoListDeleteRequest request, CancellationToken cancellationToken)
    {
        var todoList = await this.dbContext.TodoLists.FindAsync(request.Id);

        if (todoList is null)
        {
            throw new NotFoundException(nameof(TodoList), request.Id);
        }

        await this.resourceAuthorizationService.Authorize(todoList, Operations.FullAccess);

        this.dbContext.TodoLists.Remove(todoList);
        await this.dbContext.SaveChangesAsync(cancellationToken);

        return Unit.Value;
    }
}