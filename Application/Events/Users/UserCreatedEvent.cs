﻿using MediatR;

namespace TestGitlabCi.Application.Events.Users;

internal class UserCreatedEvent : INotification
{
    public UserCreatedEvent(Guid userId)
    {
        this.UserId = userId;
    }

    public Guid UserId { get; set; }
}