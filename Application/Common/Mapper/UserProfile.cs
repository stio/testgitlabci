﻿using AutoMapper;
using TestGitlabCi.Application.Contracts.Models;
using TestGitlabCi.Domain.Entities;

namespace TestGitlabCi.Application.Common.Mapper;

internal class UserProfile : Profile
{
    public UserProfile()
    {
        this.CreateMap<User, CurrentUser>();
    }
}