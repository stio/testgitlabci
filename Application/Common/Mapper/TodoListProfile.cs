﻿using AutoMapper;
using TestGitlabCi.Application.Contracts.Models;
using TestGitlabCi.Application.Contracts.Requests;
using TestGitlabCi.Domain.Entities;

namespace TestGitlabCi.Application.Common.Mapper;

public class TodoListProfile : Profile
{
    public TodoListProfile()
    {
        this.CreateMap<TodoList, TodoListDto>();

        this.CreateMap<TodoListUpdateRequest, TodoList>();
    }
}