﻿using System.Net;

namespace TestGitlabCi.Application.Common.Exceptions;

public class AccessDeniedException : HttpStatusException
{
    public AccessDeniedException(HttpStatusCode statusCode = HttpStatusCode.BadRequest)
        : base("Access denied.", statusCode)
    {
    }
}