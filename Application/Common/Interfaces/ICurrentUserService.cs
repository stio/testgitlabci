﻿using TestGitlabCi.Domain.Enums;

namespace TestGitlabCi.Application.Common.Interfaces;

public interface ICurrentUserService
{
    public Guid UserId { get; }

    public string? Email { get; }

    public bool IsAuthenticate { get; }

    public Role Role { get; }
}