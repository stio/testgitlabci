﻿using Microsoft.AspNetCore.Authentication;

namespace TestGitlabCi.Web.Mocks.Authentication;

public class MockAuthAuthenticationOptions : AuthenticationSchemeOptions
{
    public const string DefaultScheme = "MockAuth";

    public string Scheme => DefaultScheme;

    public string AuthenticationType => DefaultScheme;
}