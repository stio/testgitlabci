﻿using System.ComponentModel.DataAnnotations;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using TestGitlabCi.Application.Contracts.Common;
using TestGitlabCi.Application.Contracts.Models;
using TestGitlabCi.Application.Contracts.Requests;
using TestGitlabCi.Web.Api.Models;

namespace TestGitlabCi.Web.Controllers.V1;

public class TodoListController : Api.Controllers.V1.TodoListController
{
    private readonly ISender sender;

    public TodoListController(ISender sender)
    {
        this.sender = sender;
    }

    public override async Task<ActionResult<PagedList<TodoListRecordDto>>> SearchTodoList([Required] TodoListSearchRequest request)
    {
        return await this.sender.Send(request);
    }

    public override async Task<ActionResult<TodoListDto>> CreateTodoList([Required] TodoListCreateRequest request)
    {
        return await this.sender.Send(request);
    }

    public override async Task<ActionResult<TodoListDto>> GetTodoList(Guid todoListId)
    {
        return await this.sender.Send(new TodoListGetRequest(todoListId));
    }

    public override async Task<ActionResult<TodoListDto>> UpdateTodoList(Guid todoListId, [Required] TodoListUpdateRequest request)
    {
        if (todoListId != request.Id)
        {
            return this.NotEqualIdsResponse();
        }

        return await this.sender.Send(request);
    }

    public override async Task<ActionResult<SuccessfulResult>> DeleteTodoList(Guid todoListId)
    {
        await this.sender.Send(new TodoListDeleteRequest(todoListId));

        return this.Response200();
    }
}