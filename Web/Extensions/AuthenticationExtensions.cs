﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using TestGitlabCi.Shared.Options;
using TestGitlabCi.Web.Mocks.Authentication;

namespace TestGitlabCi.Web.Extensions;

public static class AuthenticationExtensions
{
    public static void ConfigureAuthentication(this IServiceCollection services, IConfiguration configuration)
    {
        var authSettings = configuration.GetSection(nameof(AuthSettings)).Get<AuthSettings>()!;
        services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddJwtBearer(options =>
            {
                options.Authority = authSettings.Authority;
                options.Audience = authSettings.Audience;
            });

        var useMockAuth = configuration.GetValue<bool>("USE_MOCK_AUTH");
        if (useMockAuth)
        {
            services.AddAuthentication(MockAuthAuthenticationOptions.DefaultScheme)
                .AddScheme<MockAuthAuthenticationOptions, MockAuthHandler>(MockAuthAuthenticationOptions.DefaultScheme, _ => { });
        }
    }
}