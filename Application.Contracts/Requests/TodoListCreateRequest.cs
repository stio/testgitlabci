﻿using System.ComponentModel.DataAnnotations;
using MediatR;
using TestGitlabCi.Application.Contracts.Models;
using TestGitlabCi.Domain.Enums;

namespace TestGitlabCi.Application.Contracts.Requests;

/// <summary>
/// TodoListCreateRequest
/// </summary>
public class TodoListCreateRequest : IRequest<TodoListDto>
{
    /// <summary>
    /// Name of TodoList
    /// </summary>
    /// <example>Example Name</example>
    [Required]
    [MaxLength(255)]
    public string Name { get; set; } = null!;

    /// <summary>
    /// Type of TodoList
    /// </summary>
    [Required]
    public TodoListType Type { get; set; }
}