﻿using MediatR;
using TestGitlabCi.Application.Contracts.Models;

namespace TestGitlabCi.Application.Contracts.Requests;

/// <inheritdoc />
public class CurrentUserGetRequest : IRequest<CurrentUser>
{
}