﻿using System.ComponentModel.DataAnnotations;
using TestGitlabCi.Application.Contracts.Enums;

namespace TestGitlabCi.Application.Contracts.Common;

/// <summary>
/// Sorting
/// </summary>
public class Sorting
{
    /// <summary>Name of field for sort</summary>
    [Required]
    public string Column { get; set; } = null!;

    /// <summary>Direction</summary>
    [Required]
    public DirectionType Direction { get; set; }
}