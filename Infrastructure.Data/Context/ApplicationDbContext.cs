﻿using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using TestGitlabCi.Application.Common.Interfaces;
using TestGitlabCi.Domain.Entities;
using TestGitlabCi.Infrastructure.Data.Interceptors;

namespace TestGitlabCi.Infrastructure.Data.Context;

internal class ApplicationDbContext : DbContext, IApplicationDbContext
{
    private readonly BaseEntitySaveChangesInterceptor? baseEntitySaveChangesInterceptor;

    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
        : base(options)
    {
    }

    [ActivatorUtilitiesConstructor]
    public ApplicationDbContext(
        DbContextOptions<ApplicationDbContext> options,
        BaseEntitySaveChangesInterceptor baseEntitySaveChangesInterceptor)
        : base(options)
    {
        this.baseEntitySaveChangesInterceptor = baseEntitySaveChangesInterceptor;
    }

    public DbSet<User> Users { get; set; } = null!;

    public DbSet<TodoList> TodoLists { get; set; } = null!;

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());

        base.OnModelCreating(modelBuilder);
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        if (this.baseEntitySaveChangesInterceptor is not null)
        {
            optionsBuilder.AddInterceptors(this.baseEntitySaveChangesInterceptor);
        }

        base.OnConfiguring(optionsBuilder);
    }
}