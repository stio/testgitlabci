﻿using System.Text.Json.Serialization;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.JsonMultipartFormDataSupport.Extensions;
using Swashbuckle.AspNetCore.JsonMultipartFormDataSupport.Integrations;
using TestGitlabCi.Web.Api.Conventions;
using TestGitlabCi.Web.Api.Converters;
using TestGitlabCi.Web.Api.Filters;

namespace TestGitlabCi.Web.Api.Extensions;

/// <summary>
/// Controller Extensions
/// </summary>
public static class ConfigureControllerExtensions
{
    /// <summary>
    /// Configure Controllers
    /// </summary>
    /// <param name="services"></param>
    public static void ConfigureControllers(this IServiceCollection services)
    {
        services.AddControllers(opts =>
            {
                opts.Conventions.Add(new ApiExplorerGroupPerVersionConvention());
                opts.Filters.Add<ApplicationExceptionFilter>();
            })
            .AddJsonOptions(options =>
            {
                options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
                options.JsonSerializerOptions.Converters.Add(new DateTimeJsonConverter());
                options.JsonSerializerOptions.Converters.Add(new DateOnlyJsonConverter());
                options.JsonSerializerOptions.DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull;
            })
            .UseCustomValidationErrorResponses();

        services.AddJsonMultipartFormDataSupport(JsonSerializerChoice.SystemText);
    }
}